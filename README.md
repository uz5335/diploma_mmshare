# diploma_MMSHARE

APLIKACIJA DELUJE SAMO V LOKALNEM TESTNEM OKOLJU CLOUD9 (podatkovna zbirka je lokalna) IN JE RAZVITA ZA POTREBE DIPLOMSKE NALOGE. UPORABNIK SI LAHKO NAMESTI SVOJO LOKALNO VERZIJO PROJEKTA. PRED PRVIM ZAGONOM JE POTREBNO IZBRISATI MAPO "DATA".
Repozitorij zaenkrat še ni primeren za produkcijo.

Full stack implementacija MMShare aplikacije, ki deluje na principu dropboxa za deljenje datotek. Vsak uporabnik ima razmerje izraženo v obliki virtualnega denarja. Novo vsebino lahko uporabnik
odklene s tem, da od nekoga kupi vsebino, pri tem drug uporabnik dobi virtualen denar plus se nagrado od streznika. Vse transkacije se shranjujejo v blokovno verigo...  

Naslov aplikacije:: https://diploma-denar-uz5335.c9users.io --> tukaj teče app za development..  
  
Za delovanje je potrebna podatkovna lokalna verzija MongoDB podatkovne zbirke in vsaj Node.js v 8.x


## API klici

### Uporabnik

* GET:/logicAPI/getUser/<userID>  
* POST: /logicAPI/createUser  {"username", "email", "password"}  
* POST: /logicAPI/login  {"username", "password"}  
* PUT: /logicAPI/changeDescription/<userId> {"description"}  
* PUT: /logicAPI/changePassword/<userId> {"oldPassword", "newPassword"}  
* GET: /logicAPI/getUserBalance/<userId>  
* DELETE: /logicAPI/deleteUser {"userId"}  

### Vsebina

* POST: /logicAPI/fileUpload  {"file", "userID", "topicName"}  
* GET: /logicAPI/fileDownload/<userId>/<fileId>  
* POST: /logicAPI/createTopic {"topicName"}  
* POST: /logicAPI/buyContent {"userId", "contentId", "password"}  
* DELETE: /logicAPI/deleteContent {"userId", "contentId"}  

### Iskanje

* GET: /logicAPI/searchByUserName/<userName>  
* GET: /logicAPI/searchByUserId/<userId>  
* GET: /logicAPI/searchContentInTopic/<topicName>  
* GET: /logicAPI/getAllTopics  
* GET: /logicAPI/getAllAccessTopic/<userId>  
* GET: /logicAPI/userDataByUserName/<userName>  
* GET: /logicAPI/findContentById/<contentId>  

### Vizualizacija transakcij  

* /admin --> na podstran v url-ju se premakneš na pregled poteka transakcij med uporabniki...

Posamezna vozlišča na sliki je možno premikati po platnu in si pogled prilagoditi. Če vrtiš kolešček na miški lahko zoomaš ali odzoomaš...click and drag --> premik platna levo/desno.

## MongoDB priprava okolja

$ sudo apt-get remove mongodb-org mongodb-org-server  
$ sudo apt-get autoremove  
$ sudo rm -rf /usr/bin/mongo*  
$ echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list  
$ sudo apt-get update  
$ sudo apt-get install mongodb-org mongodb-org-server  
$ sudo touch /etc/init.d/mongod  
$ sudo apt-get install mongodb-org-server  
  
$ cd ~/workspace  
$ mkdir mongodb  
$ cd mongodb  
$ mkdir data  
$ echo 'mongod --bind_ip=$IP --dbpath=data --nojournal --rest "$@"' > mongod  
$ chmod a+x mongod  

$ cd ~/workspace/mongodb  
$ ./mongod  

Bugfix za version: nvm i v8.11.1  

* npm install 
* SET PORT=3000 //set port

