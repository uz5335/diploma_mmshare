
"use strict";
function ifExist(arr, nov){
    return (arr.indexOf(nov) > -1);
}

$( document ).ready(function() {
    
    getAlLTransactions(function(rs){
        if(rs){
            //console.log(rs);
            let nodes = [];
            
            
            for(let j = 0; j < rs.length; j++){
                let dest = rs[j].destinationAddr;
                let source = rs[j].sourceAddr;
                if(!ifExist(nodes, dest)){
                    nodes.push(dest);
                }
                
                if(!ifExist(nodes, source)){
                    nodes.push(source);
                }
                
            }
            
            //console.log(nodes);
                var i,
                s,
                //N = 2,
                //E = 10,
                g = {
                  nodes: [],
                  edges: []
                };
                
                var server = "219a3240c3ca75363f2417080d17c1e5c015436ef05eca82fdd6fb410ad32965";
                
            // Generate a random graph:
            for (i = 0; i < nodes.length; i++){
                g.nodes.push({
                id: nodes[i],
                label: nodes[i] === server? "SERVER": nodes[i],
                x: Math.random(),
                y: Math.random(),
                size: 2,
                color: '#666'
              });
            }
            
            let whoWasSource = [];
              
            for(i = 0; i < rs.length; i++){
                let dest = rs[i].destinationAddr;
                let color = "green";
                
                let displayEdgeSource = rs[i].sourceAddr;
                if(rs[i].sourceAddr === server){
                    displayEdgeSource = "SERVER";
                }
                
                for(let j = 0; j < rs[i].num; j++){
                      g.edges.push({
                        id: 'e' + i + j,
                        label: "AMOUNT:  " + rs[i].amount,
                        labelSize: "10",
                        source: rs[i].sourceAddr,
                        target: rs[i].destinationAddr,
                        size: 100,
                        color: color,
                        type: 'curvedArrow',
                        count: j
                      });
                    
                }
                
            }
            
            // Instantiate sigma:
            s = new sigma({
              graph: g,
              renderer: {
                container: document.getElementById('graph-container'),
                type: 'canvas'
              },
               settings: {
                edgeLabelSize: 'proportional',
                minEdgeSize: 3.5,
                maxEdgeSize: 3.5,

                //edgeLabelColor: "red"
              }
            });
            // Initialize the dragNodes plugin:
            var dragListener = sigma.plugins.dragNodes(s, s.renderers[0]);
            
            dragListener.bind('startdrag', function(event) {
              //console.log(event);
            });
            dragListener.bind('drag', function(event) {
              //console.log(event);
            });
            dragListener.bind('drop', function(event) {
              //console.log(event);
            });
            dragListener.bind('dragend', function(event) {
              //console.log(event);
            });

        }
    });
    

    
    
});

function getAlLTransactions(callback){
          $.ajax({
            type: "GET",
            url: "adminAPI/getTransactions",
            success: function(data){
                        //console.log(data);
        				callback(data);
        			},
            statusCode: {
                500: function(err) {
                  alert(err);
                },
                404: function(err){
                    alert(err);
                  
                }
              }
            
        });
}
