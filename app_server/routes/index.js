var express = require("express");
var router = express.Router();

var renderCtrl = require("../controllers/renderCtrl.js");

router.get("/", renderCtrl.renderAdmin);

module.exports = router;