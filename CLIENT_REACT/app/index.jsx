import React from 'react';
import {render} from 'react-dom';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom';

import Main from "./components/Main.jsx";
import Login from "./components/LoginRegister/login/Login.jsx";
import Register from "./components/LoginRegister/register/Register.jsx";
import Home from "./components/Home/Home.jsx";
import SearchTopic from "./components/searchTopic/SearchTopic.jsx";
import HomeAll from "./components/Home/Home_all.jsx";
import SearchUser from "./components/searchUser/SearchUser.jsx";

class App extends React.Component {
  render () {
      
    return (
       <Router>
            
            <Switch>
                <Route exact path="/" component={Main} />
                <Route  path="/login" component={Login} />
                <Route path="/register" component={Register} />
                <Route path="/home" component={Home} />
                <Route path="/searchTopic" component={SearchTopic} />
                <Route path="/ownedFiles" component={HomeAll}/>
                <Route path="/searchUser" component={SearchUser}/>
                <Route component={NoMatch} />
            </Switch>
            
        </Router> 
    );
  }
}

class NoMatch extends React.Component {
    render(){
        return (
                <div>
                    <h1>No router found: {this.props.location.pathname}</h1>
                </div>
            );
    }
}


render(<App/>, document.getElementById('app'));