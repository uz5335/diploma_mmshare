import axios from 'axios';

export class APIZahtevki {
    constructor(){
    }
    
     static allTopics(){
        return axios({
            method: 'get',
            url: "/logicAPI/getAllTopics",
            data: {
            }
      });
    }
    
    static loginCheck(user, password){
        return axios({
            method: "post",
            url: "/logicAPI/login",
            data: {
                username: user,
	            password: password
            }
        });
    }
    static getUser(userId){
        return axios({
            method: "get",
            url: "/logicAPI/getUser/"+userId,
            data: {
                
            }
        });
    }
    
    static getUserBalance(userId){
        return axios({
            method: "get",
            url: "/logicAPI/getUserBalance/"+userId,
            data: {
                
            }
        });
    }
    static getAllTopics(){
        return axios({
            method: "get",
            url: "/logicAPI/getAllTopics",
            data: {
                
            }
        });
    }
    
    static getUserById(userId){
        return axios({
            method: "get",
            url: "/logicAPI/searchByUserId/"+userId,
            data: {
                
            },
            
        });
    }
    
    static downloadFile(fileId, userId){
        return axios({
            method: "get",
            url: "/logicAPI/fileDownload/"+userId+"/"+fileId,
            data: {
                
            },
            responseType:'arraybuffer'
        });
    }
    
    static removeFile(fileId, userId){
            return axios({
            method: "delete",
            url: "/logicAPI/deleteContent",
            data: {
            	userId: userId,
                contentId: fileId
            },
        });
    }
    
    static uploadFile(file, userId, topicName){
            let formData = new FormData();
            formData.append("file", file);
            formData.append("userID", userId);
            formData.append("topicName", topicName);
           // console.log(file, userId, topicName);
            
            return axios.post('/logicAPI/fileUpload', formData, {
                    headers: {
                      'Content-Type': 'multipart/form-data'
                    }
                })
    }
    
    static createNewTopic(topicName){
         return axios({
            method: "post",
            url: "/logicAPI/createTopic",
            data: {
                topicName: topicName
            },
        });
    }
    
    static searchAllContentInGivenTopic(topicName){
        //console.log(topicName);
        return axios({
            method: "get",
            url: "/logicAPI/searchContentInTopic/"+topicName,
        });
    }
    
    static buyContent(contentID, userId, password){
              return axios({
                method: "post",
                url: "/logicAPI/buyContent",
                data: {
                    userId: userId,
	                contentId: contentID,
	                password: password
                },
        });
    }
    
    static UserAllAccesFiles(userId){
          return axios({
            method: "get",
            url: "/logicAPI/getAllAccessTopic/"+userId,
        });
    }
    
    static GetSearchUserContent(userName){
             return axios({
            method: "get",
            url: "/logicAPI/searchByUserName/"+userName,
        });
    }
    
    /*USER CHANGES*/
    static changeDescription(userId, newDescription){
         return axios({
            method: "put",
            url: "/logicAPI/changeDescription/"+userId,
            data: {
                description: newDescription
            }
        });
    }
    
    static changePassword (oldPassword, newPassword, userId){
         return axios({
            method: "put",
            url: "/logicAPI/changePassword/"+userId,
            data: {
                	oldPassword:oldPassword,
	                newPassword:newPassword
            }
        });
        
    }
    
      static getUserDataByName(userName){
             return axios({
            method: "get",
            url: "/logicAPI/userDataByUserName/"+userName,
        });
    }
    
    static deleteUser(userId){
             return axios({
            method: "delete",
            url: "/logicAPI/deleteUser",
            data: {
                userId: userId
            }
        });
    }
    
    static registerUser(username, email, password){
          return axios({
            method: "post",
            url: "/logicAPI/createUser",
            data: {
            	username: username,
            	email: email,
            	password: password
            }
        });
    }
    
}

