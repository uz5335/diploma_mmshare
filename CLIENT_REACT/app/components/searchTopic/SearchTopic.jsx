import React, {Component} from "react"
import { Link } from 'react-router-dom';

import {localStorage} from "../../utils/localStorageUtils.js";
import {APIZahtevki} from "../../utils/api_calls.js";
import Navbar from "../commonMainComponents/Navbar.jsx";
import Contents from "../commonMainComponents/Content.jsx"
import * as qs from 'query-string';

class SearchTopic extends Component{
    constructor(props){
        super(props);
        this.state = {
            userData: [],
            balance: "",
            parsed: "",
            topics: [],
            userContent: [],
            showLoader: "none"
        }
        this.handleChange = this.handleChange.bind(this);
        this.getAllTopics = this.getAllTopics.bind(this);
        this.getTopicContent = this.getTopicContent.bind(this);
        this.toggleLoader = this.toggleLoader.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);
        this.handlePurchase = this.handlePurchase.bind(this);
        this.userBalance = this.userBalance.bind(this);
    }
    
    userBalance(){
          APIZahtevki.getUserBalance(localStorage.getLocaLocalStorage()).then(
              function (rs){
                //console.log(rs.data);
                this.setState({
                    balance: rs.data.balance - 1
                });
              }.bind(this), function(error){
                alert("no such user with given id ");
                localStorage.removeFromLocalStorage();
                this.props.history.push("/");
              }.bind(this)
          );
    }
   
    handlePurchase(contentID, password){
        //console.log(contentID);
        //console.log(password);
        APIZahtevki.buyContent(contentID, localStorage.getLocaLocalStorage(), password).then(
                function(rs){
                    this.getTopicContent(this.props);
                    this.userBalance(this.props);
                }.bind(this),function(error){
                    //console.log(error);
                    alert(error.response.data.error);
                }
            )
    }
    
     handleFileChange(){
          this.getTopicContent(this.props);
    }
    
    toggleLoader(){
        this.setState({
            showLoader: this.state.showLoader === "none" ? "block" : "none"
        });
    }
    
    componentWillMount() {
        if(!localStorage.checkLocalStorage()){
            this.props.history.push("/");
        }
    }
    
    getAllTopics(){
        APIZahtevki.getAllTopics().then(function(rs){
            this.setState({
                topics: rs.data
            });
        }.bind(this));
    }
    
    getTopicContent(nextProps){
         APIZahtevki.searchAllContentInGivenTopic(qs.parse(nextProps.location.search).search).then(
              function (rs){
                  //console.log(rs);
                this.setState({
                    userContent: rs.data
                })
              }.bind(this), function(error){
                alert("no content found")
              }.bind(this)
          );
    }
    
    componentDidMount(){
           
          APIZahtevki.getUser(localStorage.getLocaLocalStorage()).then(
              function (rs){
                //console.log(rs);
                this.setState({
                    userData: rs.data
                });
              }.bind(this), function(error){
                alert("no such user with given id ");
                localStorage.removeFromLocalStorage();
                this.props.history.push("/");
              }.bind(this)
          );
          
        this.userBalance();
        this.getAllTopics();
        this.handleChange(this.props);
    }
    
    handleChange(nextProps){
        //console.log("new state", qs.parse(this.props.location.search).search);
        this.setState({
            parsed: qs.parse(nextProps.location.search).search
        })
        this.getTopicContent(nextProps);
    }
    
     componentWillReceiveProps(nextProps){
          //console.log(nextProps);
          if(qs.parse(nextProps.location.search).search !== this.state.parsed){
              this.handleChange(nextProps);
          }
    }

      render(){
         //console.log(this.state.userContent);
        let props = {
            username:this.state.userData.username,
            balance: this.state.balance.toString(),
            topics: this.state.topics,
            history: this.props.history,
             userData: this.state.userData
        }
        
        let propsContent = {
            content: this.state.userContent, 
            history: this.props.history, 
            handleRemoveFile: this.handleFileChange, 
            toggleLoader: this.toggleLoader,
            balance: this.state.balance.toString(),
            handlePurchase: this.handlePurchase,
            userData: this.state.userData
        }
        return (
            <div>
                <div id="loader" style={{display: this.state.showLoader}}></div>
                <Navbar {...props}/>
                
                 <div id="page-content-wrapper ">
                    <div class="container-fluid">
                        <div class="row">
                            <div  align="center" class="col-sm-12 col-md-3">
                                    
                                    <div id="userdata">
                                        <h2 class="bold-topic">Topic: <span class="bold-topic-1"> {this.state.parsed}</span></h2> 
                                    </div>
                            </div>
                            
                            <div  class="col-sm-12 col-md-9">
                            
                                <div class="mainframe">
                                    <Contents {...propsContent}/>
                                </div>
                            </div>
                          </div>
                      
                    </div>
                </div>
           </div>

            );
    }
}

export default SearchTopic;



