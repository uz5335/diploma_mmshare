import React, { Component } from 'react';


const HomeLeftSide = (props) =>{
    //console.log(props.description)
    return (
            <div  align="center" className="col-sm-12 col-md-3 user">
                    <div id="userdata">
                        <h2>User data: </h2>
                    </div>
                    
                  <div className="card">
                        <div className="card-header"> 
                            WALLET
                    	</div>
                    	    <div className="card-body">
                    	        <div>
                    	            <div className="border-user">BALANCE: <span class="text-color"> {props.balance + "$"}</span></div>
                    	            <div className="border-user">doc. number: <span class="text-color"> {props.fileNumber}</span></div>
                    	        </div>
                    	    	
                    	    </div> 
                    
                	</div>
                	<br/>
            	     <div className="card">
                    	    <div className="card-body">
                    	      <form onSubmit={props.changeDescription}  className="input-vpisi" autoComplete="off">
                                  <textarea  className="form-control" id="descriptionID"  value={props.description} onChange={props.changeD}/> 
                                  <button  className="btn2" type="submit" id="btn-email">Change description</button>
                              </form>
                    	    	
                    	    </div> 
                	</div>
                	
                	<br/>
            	     <div className="card">
                    	    <div className="card-body">
                    	      <form  onSubmit={props.changeUserPassword} className="input-vpisi" autoComplete="off">
                                  <input  className="form-control" type="password" placeholder="Old password" aria-describedby="basic-addon1" id="old-pass"/>
                                  <input  className="form-control" type="password" placeholder="New password" aria-describedby="basic-addon1" id="new-pass"/>
                                  <input  className="form-control" type="password" placeholder="Confirm new password" aria-describedby="basic-addon1" id="new-pass1"/>
                                  <button className="btn2" type="submit" id="btn-pass">Change password</button>
                              </form>
                    	    	
                    	    </div> 
                	</div>
                	<br/>
            	     <div className="card">
                    	    <div className="card-body">
                    	      <form onSubmit={props.deleteUser} className="input-vpisi" autoComplete="off">
                                  <input id="check" class="form-controll" type="checkbox" name="delete" value="delete"/> Delete account?<br/>
                                  <button className="btn2" type="submit" id="btn-delete">DELETE</button>
                              </form>
                    	    	
                    	    </div> 
                	</div>
                    <br/>
                
            </div>
        
        );
}

export default HomeLeftSide;