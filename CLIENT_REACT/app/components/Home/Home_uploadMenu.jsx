import React, {Component} from "react";


import {localStorage} from "../../utils/localStorageUtils.js";
import {APIZahtevki} from "../../utils/api_calls.js";

class UploadTask extends Component{
    constructor(props){
        super(props);
        
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.handleNewTopic = this.handleNewTopic.bind(this);
        
    }
    handleFileUpload(e){
        e.preventDefault();
        var options = document.getElementById("selectTopic");
        var selectedTopic = options.options[options.selectedIndex].value;
        let file = document.querySelector('#fileId').files[0];
        //console.log(file.name);
        if(!file || selectedTopic === "Choose topic..."){
            alert("select file and topic before upload")
        }
        else if(file.size / (1024 * 1024 ) > 10){
            alert("only files smaller then 10MB allowed")
        }
        else{
            $('.bd-example-modal-lg').modal('hide');
            this.props.toggleLoader();
            APIZahtevki.uploadFile(file, localStorage.getLocaLocalStorage(), selectedTopic).then(
                    function(rs){
                        //console.log(rs);
                        this.props.toggleLoader();
                        this.props.handleFileChange();
                        
                    }.bind(this), function(error){
                        //console.log(error);
                        this.props.toggleLoader();
                        alert(error.response.data.error);
                    }.bind(this)
                );
        }
    }
    handleNewTopic(e){
        e.preventDefault();
        let topicName = document.getElementById("new-topic").value.trim();
        if(!topicName){
            alert("to create new topic you shoud first type in something")
        }else{
            //console.log("new topic: ", topicName);
            APIZahtevki.createNewTopic(topicName).then(
                function(rs){
                    this.props.getAllTopics();
                    alert("TOPIC CREATED")
                }.bind(this), function(error){
                    alert(error.response.data.error);
                }
                
                );
        }
        
    }
    
    render(){
        return(
                <div className="modal fade bd-example-modal-lg" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                   <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div className="modal-content">
                      <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLongTitle">Upload new file</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div className="modal-body">
                           <div className="container-fluid">
                                <div className="row">
                                  <div className="col-sm-7 border-modalbox">
                                      <form onSubmit={this.handleFileUpload} id="upload-form"  autoComplete="off">
                                          <input  type="file"  id="fileId"/>
                                          <OptionTopicSelect topics={this.props.topics}/>
                                          <button className="btn4"  id="nalozi">Upload</button>
                                      </form>
                                    </div>
                                  <div className="col-sm-5 ml-auto">
                                      <h5>NEW TOPIC (if you want)</h5>
                                          <form onSubmit={this.handleNewTopic}  autoComplete="off">
                                              <input  className="form-control" type="text" placeholder="Topic name" aria-describedby="basic-addon1" id="new-topic"/>
                                              <button className="btn4" type="submit" id="new-topic-create">CREATE TOPIC</button>
                                          </form>
                                  </div>
                                </div>
                             
                              </div>
                      </div>
                      <div className="modal-footer">
                        <button type="button" className="btn" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
            );
    }
}

export default UploadTask;

const OptionTopicSelect = (props)=>{
        //console.log(props);
        let options = props.topics.map((topic, i)=>{
            return (
                    <option key={i.toString()} value={topic.name}>{topic.name}</option>
                );
        });
    return (
              <select className="custom-select mb-2 mr-sm-2 mb-sm-0" id="selectTopic">
                <option defaultValue>Choose topic...</option>
                {options}
              </select>
        );
    
}