import React, { Component } from 'react';


const LeftAll = (props) =>{
    return (
            <div  align="center" className="col-sm-12 col-md-3 user">
                    <div id="userdata">
                        <h2>User data: </h2>
                    </div>
                    
                  <div className="card">
                        <div className="card-header"> 
                            WALLET
                    	</div>
                    	    <div className="card-body">
                    	        <div>
                    	            <div className="border-user">BALANCE: <span class="text-color"> {props.balance + "$"}</span></div>
                    	            <div className="border-user">doc. number: <span class="text-color"> {props.fileNumber}</span></div>
                    	        </div>
                    	    	
                    	    </div> 
                    
                	</div>
                
            </div>
        
        );
}

export default LeftAll;