import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import {localStorage} from "../../utils/localStorageUtils.js";
import {APIZahtevki} from "../../utils/api_calls.js";
import Navbar from "../commonMainComponents/Navbar.jsx";
import Contents from "../commonMainComponents/Content.jsx";
import LeftAll from "./Left_side_All.jsx";


class HomeAll extends Component {
    constructor(props){
        super(props);
        this.state = {
            userData: [],
            balance: "",
            userContent: [],
            topics: [],
            showLoader: "none"
        }
        
        this.handleFileChange = this.handleFileChange.bind(this);
        this.getUserContent = this.getUserContent.bind(this);
        this.getAllTopics = this.getAllTopics.bind(this);
        this.handlePurchase = this.handlePurchase.bind(this);
        this.toggleLoader = this.toggleLoader.bind(this);
    }
    
    componentWillMount() {
        //console.log(this.props);
        if(!localStorage.checkLocalStorage()){
            this.props.history.push("/");
        }
    }
    handlePurchase(contentID){
        console.log(contentID);
    }
    
    //drugačen API KLIC, KER POTREBUJEM VSE OD UPORABNIKA...
    getUserContent(){
        APIZahtevki.UserAllAccesFiles(localStorage.getLocaLocalStorage()).then(
                  function(rs){
                     this.setState({
                         userContent: rs.data
                     }); 
                  }.bind(this),
                  function(error){
                      alert(error);
                  }
              );
    }
    
    getAllTopics(){
        
        APIZahtevki.getAllTopics().then(function(rs){
            this.setState({
                topics: rs.data
            });
        }.bind(this));
    }
    
    handleFileChange(){
          this.getUserContent();
    }
    
    toggleLoader(){
        this.setState({
            showLoader: this.state.showLoader === "none" ? "block" : "none"
        });
    }
    
    componentDidMount(){
            //console.log("home");
          APIZahtevki.getUser(localStorage.getLocaLocalStorage()).then(
              function (rs){
                //console.log(rs);
                this.setState({
                    userData: rs.data
                });
              }.bind(this), function(error){
                alert("no such user with given id ");
                localStorage.removeFromLocalStorage();
                this.props.history.push("/");
              }.bind(this)
          );
          
          APIZahtevki.getUserBalance(localStorage.getLocaLocalStorage()).then(
              function (rs){
                //console.log(rs.data);
                this.setState({
                    balance: rs.data.balance - 1
                });
              }.bind(this), function(error){
                alert("no such user with given id ");
                localStorage.removeFromLocalStorage();
                this.props.history.push("/");
              }.bind(this)
          );
          
            this.getUserContent();
            this.getAllTopics();
             // console.log("lollol");
    }
    
    render(){
        let props = {
            username:this.state.userData.username,
            balance: this.state.balance.toString(),
            topics: this.state.topics,
            history: this.props.history,
             userData: this.state.userData
        }
        let propsLeft = {
            balance: this.state.balance.toString(),
            fileNumber: (this.state.userContent.length).toString()
        }
        
        
        let propsContent = {
            content: this.state.userContent, 
            history: this.props.history, 
            handleRemoveFile: this.handleFileChange, 
            toggleLoader: this.toggleLoader,
            balance: this.state.balance.toString(),
            handlePurchase: this.handlePurchase,
            userData: this.state.userData
        }
        //console.log(this.state.userContent); ////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return (
            <div>
                <div id="loader" style={{display: this.state.showLoader}}></div>
                
                <Navbar {...props}/>
                <div style={{opacity: this.state.showLoader === "none" ? "1" : "0.3"}} id="page-content-wrapper ">
                    <div className="container-fluid">
                        <div className="row">
                                <LeftAll {...propsLeft}/>
                               <div  class="col-sm-12 col-md-9">
                                     <div id="userdata">
                                        <h2 style={{float: "left"}}>All files you have access: </h2>
                                        
                                    </div>
                                    <div className="mainframe">
                                        <Contents {...propsContent}/>
                                    </div>
                                </div>
                        </div>
                      </div>
                </div>
           </div>

            );
    }
}

export default HomeAll;
