import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import {localStorage} from "../../utils/localStorageUtils.js";
import {APIZahtevki} from "../../utils/api_calls.js";
import Navbar from "../commonMainComponents/Navbar.jsx";
import HomeLeftSide from "./Home_leftSide.jsx";
import Contents from "../commonMainComponents/Content.jsx"
import UploadTask from "./Home_uploadMenu.jsx";

class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
            userData: [],
            balance: "",
            userContent: [],
            topics: [],
            showLoader: "none",
            description: ""
        }
        
        this.handleFileChange = this.handleFileChange.bind(this);
        this.getUserContent = this.getUserContent.bind(this);
        this.getAllTopics = this.getAllTopics.bind(this);
        this.toggleLoader = this.toggleLoader.bind(this);
        this.handlePurchase = this.handlePurchase.bind(this);
        this.changeDescription = this.changeDescription.bind(this);
        this.getUserData = this.getUserData.bind(this);
        this.changeD = this.changeD.bind(this);
        this.changeUserPassword = this.changeUserPassword.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        
        this.timer;
        this.refreshPage = this.refreshPage.bind(this);
    }
    
    componentWillMount() {
        //console.log(this.props);
        if(!localStorage.checkLocalStorage()){
            this.props.history.push("/");
        }
    }
    
    refreshPage(){
        console.log("naslednji klic");
        this.getUserContent();
    }
    
    deleteUser(e){
        e.preventDefault();
          if(document.getElementById('check').checked){
                APIZahtevki.deleteUser(localStorage.getLocaLocalStorage()).then(
                      function (rs){
                        //console.log(rs);
                        localStorage.removeFromLocalStorage();
                        this.props.history.push("/");
                      }.bind(this), function(error){
                        alert("something went terrible wrong");
                      }.bind(this)
                  );
        }else{
            alert("To delete Account click on checkbox");
        }
    }
    
    changeUserPassword(e){
        e.preventDefault();
        //console.log("click");
        let oldPass = document.getElementById("old-pass").value.trim();
        let newPass = document.getElementById("new-pass").value.trim();
        let newPass1 = document.getElementById("new-pass1").value.trim();
        if(!oldPass || !newPass || !newPass1){
            alert("No field shoud be empty");
        }else if(newPass !== newPass1){
            alert("type new pass 2x the same");
        }else if(newPass.length < 8){
            alert("new password must be at least 8 characters long");
        }
        else{
            APIZahtevki.changePassword(oldPass, newPass, localStorage.getLocaLocalStorage()).then(
                function(rs){
                    alert("PASSWORD CHANGE SUCCES");
                }.bind(this), function(error){
                    //console.log(error);
                    alert(error.response.data.error);
                }
                
                );
        }
        
        document.getElementById("old-pass").value ="";
        document.getElementById("new-pass").value = "";
        document.getElementById("new-pass1").value = "";
    }
    
    handlePurchase(contentID, password){
        
    }
    getUserData(){
         APIZahtevki.getUser(localStorage.getLocaLocalStorage()).then(
              function (rs){
                //console.log(rs);
                this.setState({
                    userData: rs.data,
                    description: rs.data.description
                });
              }.bind(this), function(error){
                alert("no such user with given id ");
                localStorage.removeFromLocalStorage();
                this.props.history.push("/");
              }.bind(this)
          );
        
    }
    changeDescription(e){
        e.preventDefault();
        let newDescription = document.getElementById("descriptionID").value.trim();
        if(!newDescription){
            alert("empty description not allowed");
        }else if(newDescription.length > 300){
            alert("description shoud be less then 300 char long");
        }else{
            APIZahtevki.changeDescription(localStorage.getLocaLocalStorage(), newDescription).then(
                    function(rs){
                        this.getUserData();
                        alert("SUCCESS DESCRIPTION CHANGE");
                    }.bind(this), function(error){
                        console.log(error);
                    }
                );
            
        }
    }
    
    getUserContent(){
        APIZahtevki.getUserById(localStorage.getLocaLocalStorage()).then(
                  function(rs){
                     this.setState({
                         userContent: rs.data
                     }); 
                  }.bind(this),
                  function(error){
                      alert(error);
                  }
              );
    }
    
    changeD(e){
        this.setState({
            description: e.target.value
        })
    }
    
    getAllTopics(){
        
        APIZahtevki.getAllTopics().then(function(rs){
            this.setState({
                topics: rs.data
            });
        }.bind(this));
    }
    
    handleFileChange(){
          this.getUserContent();
    }
    
    toggleLoader(){
        this.setState({
            showLoader: this.state.showLoader === "none" ? "block" : "none"
        });
    }
    
    componentDidMount(){
            //console.log("home");
            this.getUserData();
          APIZahtevki.getUserBalance(localStorage.getLocaLocalStorage()).then(
              function (rs){
                //console.log(rs.data);
                this.setState({
                    balance: rs.data.balance - 1
                });
              }.bind(this), function(error){
                alert("no such user with given id ");
                localStorage.removeFromLocalStorage();
                this.props.history.push("/");
              }.bind(this)
          );
          
            this.getUserContent();
            this.getAllTopics();
            this.timer = setInterval(this.refreshPage, 3000);
             // console.log("lollol");
    }
    
     componentWillUnmount(){
        clearInterval(this.timer);
    }
    
    render(){
        let props = {
            username:this.state.userData.username,
            balance: this.state.balance.toString(),
            topics: this.state.topics,
            history: this.props.history,
           userData: this.state.userData
            
        }
        let propsLeft = {
            balance: this.state.balance.toString(),
            fileNumber: (this.state.userContent.length).toString(),
            userData: this.state.userData,
            changeDescription: this.changeDescription,
            changeD: this.changeD,
            description: this.state.description,
            changeUserPassword: this.changeUserPassword,
            deleteUser: this.deleteUser
        }
        
        let propsUpload = {
            topics: this.state.topics,
            getAllTopics: this.getAllTopics,
            handleFileChange: this.handleFileChange,
            toggleLoader: this.toggleLoader
        }
        
        let propsContent = {
            content: this.state.userContent, 
            history: this.props.history, 
            handleRemoveFile: this.handleFileChange, 
            toggleLoader: this.toggleLoader,
            balance: this.state.balance.toString(),
            handlePurchase: this.handlePurchase,
            userData: this.state.userData
        }
        //console.log(this.state.userData);
        //console.log(this.state.userContent); ////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //console.log(this.state.description);
        return (
            <div>
                <div id="loader" style={{display: this.state.showLoader}}></div>
                
                <Navbar {...props}/>
                <div style={{opacity: this.state.showLoader === "none" ? "1" : "0.3"}} id="page-content-wrapper ">
                    <div className="container-fluid">
                        <div className="row">
                            <HomeLeftSide {...propsLeft}/>
                               <div  class="col-sm-12 col-md-9">
                                     <div id="userdata">
                                        <h2 style={{float: "left"}}>Your uploaded files: </h2>
                                        <button className="btn1" type="submit" id="btn-upload" data-toggle="modal" data-target=".bd-example-modal-lg"> <img src="images/upload.svg" width="50" height="50"  alt=""/>Upload file</button>
                                    </div>
                                    <div className="mainframe">
                                        <Contents {...propsContent}/>
                                    </div>
                                </div>
                        </div>
                      </div>
                </div>
                
                <UploadTask {...propsUpload}/>
                
           </div>

            );
    }
}

export default Home;
