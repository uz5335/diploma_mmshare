import React, { Component } from 'react';

import {localStorage} from "../../../utils/localStorageUtils.js";
import {APIZahtevki} from "../../../utils/api_calls.js";
import NavbarLoginRegister from "../commonComponents/navbarLoginRegister.jsx";
import LoginRegisterPicture from "../commonComponents/LoginRegisterPicture.jsx";


class Login extends Component {
    constructor(props){
        super(props);
        this.state={
            neki: "Login"
        }
        this.onEvent = this.onEvent.bind(this);
    }
    
    onEvent(e){
        e.preventDefault();
        let userName = document.getElementById("login-username").value.trim();
        let password =document.getElementById("login-password").value.trim();
        
        if(!userName || !password){
            alert("username and passwod fielad shoud not be emty");
            
        }else{
            
              APIZahtevki.loginCheck(userName, password).then(
                  function (rs){
                   // console.log(rs);
                    localStorage.setLocalStorage(rs.data._id);
                    this.props.history.push("/home");
                  }.bind(this), function(error){
                    alert("Incorect username or password");
                  }
              );
        }
        
         document.getElementById("login-username").value = ""
         document.getElementById("login-password").value = "";
        
    }
    
    componentWillMount() {
        if(localStorage.checkLocalStorage()){
            this.props.history.push("/home");
        }
    }
    
    render() {
            return (
                <div>
                    <NavbarLoginRegister whereToLink="/register"/>
                    <div className="container-fluid">
                      <div className="row">
                        <div  className="col-sm-12 col-md-6 center-block text-center">
                            <div id="left-block">
                             
                              <h1 className="login-text">Log In!</h1>
                                
                              <div id="left-form">
                                <form  class="input-vpisi" autoComplete="off">
                                
                                  <input  className="form-control" type="text" placeholder="Username" aria-describedby="basic-addon1" id="login-username"/>
                                  <input  className="form-control" type="password" placeholder="Password" aria-describedby="basic-addon1" id="login-password"/>
                                   
                                  <button onClick={this.onEvent} className="btn" type="submit" id="btn-login">{this.state.neki}</button>
                              </form>
                              </div>
                            </div>
                        </div>
                        
                        <LoginRegisterPicture/>
                      </div>
                    </div>
                
                </div>
            );
        }
}

export default Login;
