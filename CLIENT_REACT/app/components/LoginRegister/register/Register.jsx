import React, { Component } from 'react';

import NavbarLoginRegister from "../commonComponents/navbarLoginRegister.jsx";
import LoginRegisterPicture from "../commonComponents/LoginRegisterPicture.jsx";
import {APIZahtevki} from "../../../utils/api_calls.js";

class Register extends Component {
    constructor(props){
        super(props);
        
        this.registerUser = this.registerUser.bind(this);
    }
    //nasel na spletu
     validateEmail(email) 
        {
          var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
          return re.test(email);
        }
    
    registerUser(e){
        e.preventDefault();
        let username = document.getElementById("register-username").value.trim();
        let email = document.getElementById("register-email").value.trim();
        let pass1 = document.getElementById("register-password1").value.trim();
        let pass2 = document.getElementById("register-password2").value.trim();
        if(!username || !email || !pass1 || !pass2){
            alert("No empty field allowed");
        }else if(!this.validateEmail(email)){
            alert("invalid email format");
        }else if(pass1 !== pass2){
            alert("You shoud enter the same password twice");
        }else if(pass1.length < 8){
            alert("password shoud be at leat 8 characters long");
        }
        else{
            APIZahtevki.registerUser(username, email, pass1).then(
                function(rs){
                    //console.log(rs);
                    alert("SUCCESS. GO TO LOGIN TO ENTER YOUR ACC");
                }.bind(this), function(error){
                    alert(error.response.data.error);
                    console.log(error);
                }
                
                );
        }
        document.getElementById("register-username").value = "";
        document.getElementById("register-email").value = "";
        document.getElementById("register-password1").value = "";
        document.getElementById("register-password2").value = "";
    }
    
    render(){
        return(
            <div>
                <NavbarLoginRegister  whereToLink="/login"/>
                
                <div className="container-fluid ">
                      <div className="row">
                        <div  className="col-sm-12 col-md-6 center-block text-center">
                            <div id="left-block1">
                              
                                <h1>Register!</h1>
                            
                              <div id="left-form" class="login-text">
                                <form onSubmit= {this.registerUser} className="input-vpisi"  autoComplete="off">
                                  <input  className="form-control" type="text" placeholder="username" aria-describedby="basic-addon1" id="register-username"/>
                                    <input  className="form-control" type="email" placeholder="email" aria-describedby="basic-addon1" id="register-email"/>
                                  <input  className="form-control" type="password" placeholder="Password" aria-describedby="basic-addon1" id="register-password1"/>
                                  <input  className="form-control" type="password" placeholder=" Password" aria-describedby="basic-addon1" id="register-password2"/>
                                  <button className="btn" type="submit" id="btn-login">Register</button>
                              </form>
                              </div>
                          
                            </div>
                            
                        </div>
                            <LoginRegisterPicture/>
                      </div>
                    </div>
                
                
            </div>
            );
    }
    
}

export default Register;
