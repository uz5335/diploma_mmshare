import React, { Component } from 'react';
import {Link} from 'react-router-dom';

const LoginRegisterPicture = () => {
    return (
            <div className="col-sm-12 col-md-6 center-block text-center">
                  <img className="login-register-image" src="images/fileUpload.png"></img>
            </div>
        );
}

export default LoginRegisterPicture;