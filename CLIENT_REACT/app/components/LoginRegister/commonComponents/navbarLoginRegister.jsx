import React from 'react';
import { Link } from 'react-router-dom';


 const NavbarLoginRegister = (props) => {
     
  return (<nav className="navbar fixed-top">
                <Link to="/">
                      <span className="navbar-brand" >
                        <img src="images/favicon.ico" width="50" height="50" class="d-inline-block align-top" alt=""/>
                        <div className="center">
                        	MMShare
                        </div>
                      </span>
                  </Link>
                
                  <ul className="navbar-nav ml-auto">
                    <li className="nav-item dropdown">
                      <Link to={props.whereToLink}>
                            <span className="nav-link "> {props.whereToLink == "/register" ? "Register" : "Login"}</span>
                      </Link>
                    </li>
                  </ul>
                </nav>);
}


export default NavbarLoginRegister;