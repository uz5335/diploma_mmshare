import React, { Component } from 'react';
import {Link} from 'react-router-dom';

const NavbarDisplay = (props) => {
    return (
            <div className="pustiprostor">
                <nav className="navbar fixed-top">
                        <div className="container-fluid">
                              <div className="col-md-3.col-sm-12">
                                <div className="navbar-header">
                                  <span onClick={props.handleSideBar} id="menu-toggle"> <img  width="50px"  src="images/navigation.png"/> </span>
                                   <Link to="/home">
                                        <span id="nav-brand" className="navbar-brand">
                                          <img src="images/favicon.ico" width="50" height="50" class="d-inline-block" alt=""/>
                                          	MMShare
                                        </span>
                                    </Link>
                                   </div>
                              </div>
                              
                        <div className="col-md-3.col-sm-12">
                            
                            <form onSubmit={props.handleSubmit} className="navbar-form navbar-left">
                              <div className="input-group">
                                <input type="text" class="form-control" placeholder="@user #topic" id="searchAction" name="search"/>
                              </div>
                          </form>
                      </div>
                      
                      <div align="center"className=" navbar-right ">
                             <span id="money">{props.balance + "$"}</span>
                             <div id="imgandtext">
                                <img  width={props.koliko} src="images/user.svg"/>
                                <div className="dropdown inline-dorpbdown">
                                  <span className="btn-dropdown dropdown-toggle "  id="dropdownMenuButton " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                     {props.username}
                                  </span>
                                  <div style={{right: 0, left: "auto"}} class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <Link to="/home">
                                    <span className="dropdown-item">HOME</span>
                                    </Link>
                                    <Link to="/ownedFiles">
                                      <span className="dropdown-item" >OWNED FILES</span>
                                    </Link>
                                    <span onClick={props.handleLogOut} className="dropdown-item">LOG OUT</span>
                                  </div>
                                </div>          
                            </div>
                            
                        </div>
                        
                            
                          
                      </div>
                </nav>
                
                 <div id="wrapper" className={props.isTogled == true ? "toggled" : ""}>
  
                        <div id="sidebar-wrapper">
                           
                            <TopicList topics={props.topics} toggleSideBar= {props.handleSideBar} handleSideBarSearch={props.handleSideBarSearch}/>,

                        </div>
                        
                      
                  </div>
            
            
            </div>
        );
}

export default NavbarDisplay;


const TopicList = (props) => {
  const topics = props.topics;
  //console.log("Printam: ", topics);
  const listItems = (topics.map((topic, i) =>
    <li key={i.toString()}>
        <span data-search1={topic.name} onClick={props.handleSideBarSearch}>
            <span onClick={props.toggleSideBar}>{topic.name}</span>
      </span>
    </li>
  ));
  
  return (
    <ul className="sidebar-nav">
         <li className="sidebar-brand">
            CHOOSE TOPIC
        </li>
        {listItems}
    </ul>
  );
}