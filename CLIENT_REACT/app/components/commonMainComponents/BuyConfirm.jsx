import React, {Component} from "react";


const BuyConfirm = (props)=>{
      //console.log(props);
    return(
         <div class="modal fade bd-buy-modal-lg" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
           <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">CONFIRM PURCHASE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                   <div class="container-fluid">
                        <div class="row">
                          <div class="col-sm-3 border-modalbox">
                              {props.content.content.name}
                            </div>
                          <div class="col-sm-9 ml-auto">
                              <form  autoComplete="off">
                                  <input  class="form-control" type="password" placeholder="your password" aria-describedby="basic-addon1" id="confirm-buy-password"/>
                              </form>
                          </div>
                        </div>
                      </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                 <button  onClick={props.handlePurchaseContent} class="btn"  id="buy-content">Buy Confirm</button>
              </div>
              
            </div>
          </div>
        </div>
        
        );
    
}


export default BuyConfirm;