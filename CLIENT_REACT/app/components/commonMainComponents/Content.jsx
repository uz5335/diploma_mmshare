import React, {Component} from 'react';
var fileDownload = require('js-file-download');
import {localStorage} from "../../utils/localStorageUtils.js";
import {APIZahtevki} from "../../utils/api_calls.js";
import BuyConfirm from "./BuyConfirm.jsx"

class Contents extends Component {
    constructor(props){
        super(props);
        
        this.state = {
            content: {
                id: "",
                name: ""
            }
        }
        this.handleTopicSearch= this.handleTopicSearch.bind(this);
        this.fileDownload = this.fileDownload.bind(this);
        this.choose=this.choose.bind(this);
        this.removeFile = this.removeFile.bind(this);
        this.handleWantedPurchase = this.handleWantedPurchase.bind(this);
        this.handlePurchaseContent = this.handlePurchaseContent.bind(this);
        this.userSearch = this.userSearch.bind(this);
    }
    
    handleWantedPurchase(e){
        //console.log(e.currentTarget.getAttribute("data-id"));
        //console.log(e.currentTarget.getAttribute("data-name"));
        this.setState({
            content: {
                id: e.currentTarget.getAttribute("data-id"),
                name: e.currentTarget.getAttribute("data-name")
            }
        });
    }
    
    handlePurchaseContent(){
        let password = document.getElementById("confirm-buy-password").value;
        //console.log(password);
        if(!password){
            alert("PASSWORD REQUIRED");
        }else{
            
            this.props.handlePurchase(this.state.content.id, password);
            $('.bd-buy-modal-lg').modal('hide');
        }
        document.getElementById("confirm-buy-password").value = "";
    }
    
    handleTopicSearch(e){
        this.props.history.push('/searchTopic?search='+e.currentTarget.getAttribute("data-topic"));
    }
    
    userSearch(e){
        //console.log(this.props.userData)
        if(this.props.userData.username === e.currentTarget.getAttribute("data-topic")){
            this.props.history.push('/home');
        }else{
            this.props.history.push('/searchUser?search='+e.currentTarget.getAttribute("data-topic"));
        }
    }
    fileDownload(e){
        console.log(e.currentTarget.getAttribute("data-id"));
        console.log("localStorage: ", localStorage.getLocaLocalStorage());
        let fileName = e.currentTarget.getAttribute("data-name").toString();
        this.props.toggleLoader();
        APIZahtevki.downloadFile(e.currentTarget.getAttribute("data-id"), localStorage.getLocaLocalStorage()).then(
            function(rs){
                //console.log("lol")
                //console.log(rs);
                fileDownload(rs.data, fileName);
                this.props.toggleLoader();
            }.bind(this), 
            function(error){
                alert(error);
            }
            
            );
    }
    
    choose(accesArray){
        if(accesArray[0].userID === localStorage.getLocaLocalStorage()){
            return 1
        }
        for(let i = 1; i < accesArray.length; i++){
            if(accesArray[i].userID === localStorage.getLocaLocalStorage()){
                return 2;
            }
        }
        return 3;
    }
    removeFile(e){
        //console.log(e.currentTarget.getAttribute("data-id"));
        //console.log(this.props.handleRemoveFile);
         this.props.toggleLoader();
            APIZahtevki.removeFile(e.currentTarget.getAttribute("data-id"), localStorage.getLocaLocalStorage()).then(
                function(rs){
                    //console.log(rs);
                    this.props.toggleLoader();
                    this.props.handleRemoveFile();
                }.bind(this), 
                function(error){
                    console.log(error);
                    alert(error);
                    this.props.toggleLoader();
                }
            
            );
    }
    
    render(){
        //console.log(this.props);
        let allProps = {
            content: this.props.content,
            contentSearch: this.handleTopicSearch,
            fileDownload: this.fileDownload,
            choose: this.choose,
            removeFile: this.removeFile,
            balance: this.props.balance,
            handleWantedPurchase: this.handleWantedPurchase,
            userSearch: this.userSearch
        }
        let buyProps = {
            content: this.state,
            handlePurchaseContent: this.handlePurchaseContent,
        }
        return (
            <div>
                <OneContent {...allProps}/>	    
                <BuyConfirm {...buyProps}/>
            </div>
            );
    }
    
}

export default Contents;

const OneContent = (props)=>{
        //console.log(props);
        let contents = props.content;
       // console.log(contents);
        const contentList = contents.map((content, i) =>{
            let levoLevoOdvisno;
            //console.log(content);
            switch(props.choose(content.access)){
                case 1: //prijavljen uporabnik je lastnik;
                    //console.log("1");
                   levoLevoOdvisno = (
               	       <div  class="levo levo-levo ">
            	           <div  data-name={content.fileName} data-id={content._id} onClick={props.fileDownload} class="prenesi search-up-cat">
            	                <p style={{textAlign: "center"}}><img src="images/download.png" width="40%" height="50%"  alt=""/></p>
            	           </div>
            	           <div data-id={content._id}  onClick={props.removeFile} class="odstrani search-up-cat">
            	                <p style={{textAlign: "center"}}><img src="images/delete.png" width="40%" height="50%"  alt=""/></p>
            	           </div>
            	       </div>
                       
                       ); 
                    break;
                case 2: //prijavljen uporabnik ima samo dostop
                       levoLevoOdvisno = (
               	       <div data-name={content.fileName} data-id={content._id} onClick={props.fileDownload} class="levo levo-levo search-up-cat">
            	           <div class="napis1">
            	                 <p style={{textAlign: "center"}}><img src="images/download.png" width="40%" height="50%"  alt=""/></p>
            	           </div>
            	       </div>
                       
                       ); 
                
                    break;
                case 3: //prijavljen uporabnik, se nima dostopa do datoteke..
                         if(content.price > Number(props.balance)){
            	                  levoLevoOdvisno = (
                               	       <div class="levo levo-levo">
                            	           <div  class="prenesi">
                                	                <span style=  {{fontWeight: "bold"}}> Price:  </span>{content.price.toString()}
                                	           </div>
                                	           <div  class="odstrani" >
                                	                <span style=  {{fontWeight: "bold"}}> No coins  </span>
                                	           </div>
                            	       </div>
                            	       )
            	       }else{
            	           
            	           levoLevoOdvisno = (
                               	         <div  class="levo levo-levo ">
                                	           <div  class="prenesi">
                                	                <span style=  {{fontWeight: "bold"}}> Price:  </span>{content.price.toString()}
                                	           </div>
                                	           <div data-name={content.fileName} data-id={content._id} onClick={props.handleWantedPurchase} class="odstrani search-up-cat " data-toggle="modal" data-target=".bd-buy-modal-lg">
                                	                <p style={{textAlign: "center"}}><img src="images/finance.svg" width="40%" height="50%"  alt=""/></p>
                                	           </div>
                            	         </div>
                            	       
                            	       )
            	           
            	           
            	       }
                           
                    
                    break
            }
            
          return     (<div key={i.toString()} class="row main-margin">
            	       <div class="levo levo-levo">
            	           <div class="napis">
            	               {content.fileName}
            	           </div>
            	           
            	       </div>
            	       <div data-topic={content.creator.username} onClick={props.userSearch} class="levo levo-levo search-up-cat user-search">
            	             <div class="napis">
            	               {content.creator.username}
            	           </div>
            	       </div>
            	       <div data-topic={content.topicID.name} onClick={props.contentSearch} class="levo levo-levo search-up-cat topic">
            	             <div class="napis">
        	                   {content.topicID.name}
            	           </div>
            	       </div>
            	       
                        {levoLevoOdvisno}
    
                </div>)
        }
        );
        
        //console.log(contentList);
        
        
    return(
            <div>
                <div class="row main-margin">
            	       <div class="levo1 levo-levo">
            	           <div class="napis">
            	              <h4>File </h4>
            	           </div>
            	           
            	       </div>
            	       <div  class="levo1 levo-levo">
            	             <div class="napis">
            	               <h4>User </h4>
            	           </div>
            	       </div>
            	       <div  class="levo1 levo-levo ">
            	             <div class="napis">
        	                   <h4>Topic </h4>
            	           </div>
            	       </div>
            	       
        	       </div>
            
            
                {contentList}
            </div>
 	       
        
        );
    
}