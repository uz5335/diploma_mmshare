import React, {Component} from "react";
import { Link,  Redirect } from 'react-router-dom';

import {localStorage} from "../../utils/localStorageUtils.js";
import {APIZahtevki} from "../../utils/api_calls.js";

import NavbarDisplay from "./NavbarDisplay.jsx"

class Navbar extends Component{
    constructor(props){
        super(props);
        this.state={
            isLogin: true,
            isTogled: false,
            topics: []
        }
        this.handleLogOut = this.handleLogOut.bind(this);
        this.handleSideBar = this.handleSideBar.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSideBarSearch= this.handleSideBarSearch.bind(this);
    }
    
    handleLogOut(){
        if(confirm("Confirm logOut")){
            localStorage.removeFromLocalStorage();
            this.setState({
                isLogin: false
            });
        }
    }
    
    handleSideBar(){
      this.setState({
        isTogled: !this.state.isTogled
      });
    }
    
    handleSubmit(e) {
       e.preventDefault();
       let searchString = document.getElementById("searchAction").value.trim(); 
       //console.log(searchString.substr(1));
        if(!searchString){
            alert("type something in search field");
        }else if(searchString.charAt(0) !== "@" && searchString.charAt(0) !== "#"){
            alert("search by # or @");
        }else if(searchString.length <= 1){
            alert("# or @ is not enough for search");
        }else{
            if(searchString.charAt(0) === "@"){
                    console.log("lol1");
                    if(this.props.userData.username === searchString.substr(1)){
                        this.props.history.push('/home');
                    }else{
                        console.log("lol");
                        this.props.history.push('/searchUser?search='+searchString.substr(1));
                    }
            }else{
                console.log("lol2");
                this.props.history.push("/searchTopic?search="+searchString.substr(1));
            }
        }
    }
    
    handleSideBarSearch(e){
        //console.log("lol: ", e.currentTarget.getAttribute("data-search1"));
        //console.log(this.props);
        this.props.history.push("/searchTopic?search="+e.currentTarget.getAttribute("data-search1"));
    }
    
    render(){
        let koliko = "";
        if(this.props.length > 7){
            koliko = "10%";
        }else{
            koliko = "20%";
        }
        
        let props = {
            koliko: koliko,
            handleSideBar: this.handleSideBar,
            handleSubmit: this.handleSubmit,
            balance: this.props.balance,
            username: this.props.username,
            handleLogOut: this.handleLogOut,
            isTogled: this.state.isTogled,
            topics: this.props.topics,
            handleSideBarSearch: this.handleSideBarSearch
        }
        
        if(this.state.isLogin){
                 return ( 
                     <NavbarDisplay {...props}/>

                )
        }else{
             return <Redirect to='/'/>;
        }
    
    }
}

export default Navbar;



