import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import {localStorage} from "../utils/localStorageUtils.js";

class Main extends Component {
    render(){
        if(localStorage.checkLocalStorage()){
             return <Redirect to='/home'/>;
        }else{
            return <Redirect to='/login'/>;
        }
    }
    
}

export default Main;