const mongoose = require("mongoose");
const User = mongoose.model("User");
var fs = require("fs");
const filename = "data/1/transactions.json";

let findObject = function(arr, findAddr){
    return arr.find(o => o.addressId === findAddr);
}

module.exports.allTransactions = function(req, res){
        let output = [];
        //let labelMapping = [];
        
        User.find({}).select("addressId username").exec(function(error, users){
            if(error){
                res.status(400).json(error);
            }else{
                //console.log(users);
                fs.readFile(filename, "utf8", function(err, data) {
                    if (err){
                        console.log(err);
                    }else{
                        let json = JSON.parse(data);
                        
                        for(let i = 0;  i < json.length; i++){
                            let destAddress = json[i].data.outputs[0].address;
                            let sourceAddress = json[i].data.outputs[1].address;
                            let amount = json[i].data.outputs[0].amount;
                            
                            let destObj = findObject(users, destAddress);
                            if(destObj){
                                destAddress = destObj.username;
                            }
                            
                            let sourceObj = findObject(users, sourceAddress);
                            if(sourceObj){
                                sourceAddress = sourceObj.username;
                            }
                            
                            //poisci v outputu, ce ze imam objekt v taki kombinaciji...
                            let obstaja = false;
                            for(let j = 0; j < output.length; j++){
                                if(output[j].sourceAddr === sourceAddress && output[j].destinationAddr === destAddress){
                                    output[j].num += 1;
                                    output[j].amount += amount;
                                    obstaja = true;
                                    break
                                }
                            }
                            
                            if(!obstaja){
                                output.push({
                                    sourceAddr: sourceAddress,
                                    destinationAddr: destAddress,
                                    num: 1,
                                    amount:  amount
                                });
                            }
                            //console.log("korak: ", i);
                        }
                        
                        //console.log("KONEC");
                        console.log(output);
                        res.status(200).json(output);
                    }
                
                });
            }
        });
        

    
}