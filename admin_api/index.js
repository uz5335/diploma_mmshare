
var express = require('express');
var router = express.Router();

const adminCtrl = require("./adminCtrl.js");

//user controll
router.get('/getTransactions', adminCtrl.allTransactions);


module.exports = router;