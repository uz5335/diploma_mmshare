const mongoose = require("mongoose");
const User = mongoose.model("User");
const Topic = mongoose.model("Topic");
const Content = mongoose.model("Content");
const request = require('request');
const fs = require("fs");
const commonFunctions = require("../commonFunctions");
const apiParameters = commonFunctions.apiParameters;
const mainWallettParams = commonFunctions.mainWallettParams;

var bcrypt = require('bcrypt');
var path = require('path')

const findTopicId = function(topicName, cb){
    Topic.findOne({name: topicName}).select("_id").exec(function(error, topic){
        if(error){
            cb(error, true);
        }else if(!topic){
            cb(null, null);
        }else{
            cb(topic._id, null);
        }
    });
}
//Pri nalaganju ne dobim denarja....denar dobim, če kdo dejansko kupi moj izdelek...
module.exports.fileUpload = function(req, res){
    //console.log(req.body.userID);
    let userID = req.body.userID;
    let topicName = req.body.topicName;
     let file = req.files.file;
    console.log("userId: ", userID);
     console.log("topic name: ", topicName);
      console.log("file: ", file);
    //res.status(200).json({ok: "ok"});

    if (!req.files || !userID || !topicName || !file){
      return res.status(400).json({error: 'No files were uploaded. Missing upload parameters'});
    }else{
        if(file.data.length/1000 > 10000){
            return res.status(400).json({error: "Max upload size is 15MB"});
        }else if(!checkAllowedFileType(path.extname(file.name))){
            return res.status(400).json({error: "Only files with .docx, .xlsx, .pdf, .jpg, .png ,.txt, .pptx extension allowed"});
        }else{
            
            Content.findOne({fileName: file.name}).select("fileName").exec(function(error, content){
                if(content){
                    res.status(400).json({ok: "file with the same name already exist...change file name or format"});
                }else if(error){
                    res.status(500).json(error);
                }else{
                    commonFunctions.searchUserById(userID, function(response, error){
                        if(response && !error){
                           res.status(500).json(response);
                        }else if(!response && !error){
                            res.status(400).json({error: "can't find user with given id"})
                        }else{
                            let userId = response._id;
                            let topicID;
                            findTopicId(topicName, function(response1, flag){
                                if(!response1 && !flag){
                                    res.status(400).json({error: "can't find given topic for file upload"});
                                }else if(response1 && flag){
                                    res.status(500).json(response1);
                                }else{
                                    topicID = response1;
                                    //console.log("USERID:  ", userId);
                                    //console.log("Topic::  ", topicID);
                                    let filePrice = Math.round(file.data.length/1000);
                                    if(filePrice === 0){
                                        filePrice = 1; //dam na default vsaj en coin vredno, torej 1MB po defaultu.
                                    }
                                   // makeTransaction(mainWallettParams.walletId, mainWallettParams.addressId, response.addressId, filePrice, mainWallettParams.password, function(uspeh){
                                       // if(uspeh){
                                            let newContent = new Content({
                                                creator: userId,
                                                topicID: topicID,
                                                fileName: file.name,
                                                price: filePrice,
                                                mimetype: file.mimetype,
                                                access:[{
                                                        userID: userId,
                                                        timeAssigned: new Date() 
                                                    }],
                                                timeCreated: new Date()
                                            });
                                            newContent.save(function(err, savedTopic){
                                                if(err){
                                                    res.status(500).json(err);
                                                }else{
                                                    Topic.findById(topicID).exec(function(error, topic){
                                                        topic.contentTable.push({
                                                            _id: savedTopic._id,
                                                            userId: response._id,
                                                            timeAdded: new Date()
                                                        });
                                                        topic.save(function(err1, savedTopic){
                                                            if(err1){
                                                                res.status(500).json(err1);
                                                            }else{
                                                                //console.log(Math.round(file.data.length/1000));
                                                                //console.log("Mime type: ", typeof file.mimetype);
                                                                file.mv("logic_api/contentFiles/"+userId+"/"+file.name, function(err) {
                                                                    if (err){
                                                                      return res.status(500).json(err);  
                                                                    }else{
                                                                        res.status(201).json({ok: 'File uploaded!'});    
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                       // }else{ ////////////////
                                         //   res.status(500).json({error: "transaction failed"});
                                        //}///////////////////////
                                    //});
                                }
                            });
                        }
                    });
                }
            });

        }

    }
}

module.exports.fileDownload = function(req, res){
    let fileId = req.params.fileId;
    let userId = req.params.id; //userID od tistega, ki je ustvaril datoteko...
    
    if(!fileId || !userId){
        res.status(400).json({error: "params for file download not given"});
    }else{
        Content.findById(fileId).exec(function(error, file){
            if(!file){
                res.status(404).json({error: "cna't find specified file"});
            }else if(error){
                res.status(500).json(error);
            }else{
                let accessToFile = file.access;
                let canDownload = false;
                for(let i = 0; i < accessToFile.length; i++){
                    if(accessToFile[i].userID == userId){
                        canDownload = true;
                        break;
                    }
                }
                if(canDownload){
                    let downloadFile = "logic_api/contentFiles/" + file.creator+ "/"+file.fileName;
                    console.log("DOWNLOADING!!!!!");
                    res.download(downloadFile, file.fileName, function(err){
                        if(err){
                            console.log(err);
                            res.status(404).json({error: "no such file or directory"});
                        }
                    });
                    
                    res.attachment(downloadFile);
                    
                }else{
                    res.status(403).json({error: "access denied"})
                }
            }
            
        });
    }

}

module.exports.createTopic = function(req, res){
    let topicName = req.body.topicName;
    if(!topicName){
        res.status(400).json({error: "no topis name given"});
    }else{
        Topic.findOne({name: topicName}).exec(function(error, topic){
            if(error){
                res.status(500).json(error);
            }else if (topic){
                res.status(403).json({error: "topic name already exist"});
            }else{
                //sedaj sem zihr da lahko naredim novo temo
                let newTopic = new Topic({name: topicName, timeCreated: new Date()});
                newTopic.save(function(err, saveTopic){
                    if(err){
                        res.status(500).json(err);
                    }else{
                        res.status(201).json(saveTopic);
                    }
                });
            }
        });
    }
}

module.exports.buyContent = function(req, res){
    let userID = req.body.userId;
    let contentID = req.body.contentId;
    let password = req.body.password;
    
    if(!userID || ! contentID || !password){
        res.status(404).json({error: "missing reqeust parameters"});
    }else{
         commonFunctions.searchUserById(userID, function(response, error){
            if(response && !error){
                res.status(500).json(response);
            }else if(!response && !error){
                res.status(400).json({error: "can't find user"})
            }else{
                //PREVERI VELJAVNOST GESLA...
                bcrypt.compare(password,  response.password, function(err, res1) {
                        if(res1){
                            //Delaj vse ostalo naprej.
                            Content.findById(contentID).exec(function(err, content){
                                if(!console){
                                    res.status(404).json({error: "can't find content with given id"});
                                }else if(err){
                                    res.status(500).json(err);
                                }else{
                                    commonFunctions.searchUserById(content.creator, function(response1, error1) {
                                        if(response1 && !error1){
                                            res.status(500).json(response1);
                                        }else if(!response1 && !error1){
                                            res.status(400).json({error: "can't find user1"})
                                        }else{
                                            //poglej, ce ze imam dostop do vsebine....--> da jo ne moreš kupit 2x
                                            alreadyAcces(content.access, response._id, function(uspeh){
                                                if(uspeh){
                                                    res.status(403).json({error: "already have access to content...download it"})
                                                }else{
                                                    commonFunctions.getBalanceFunction(response.addressId, function(body){
                                                        if(body){
                                                            if(body.balance > content.price){
                                                                //ko nekdo kupi izdelek gre denar k drugemu uporabniku...
                                                                makeTransaction(response.walletId, response.addressId, response1.addressId, content.price, password, function(uspeh) {
                                                                    if(uspeh){
                                                                        //res.status(200).json({ok: "ok"})
                                                                        //dodaj zapis v bazo...
                                                                        content.access.push({
                                                                            userID: userID,
                                                                            timeAssigned: new Date()
                                                                        });
                                                                        content.save(function(err3, savedContent){
                                                                            if(err3){
                                                                                res.status(500).json({error: "problem with saveing new acces schema"})
                                                                            }else{
                                                                                
                                                                                //ko se vse zgodi uspesno se glavna denarnica da uporabniku 301 coina nagrade...
                                                                                 makeTransaction(mainWallettParams.walletId, mainWallettParams.addressId, response1.addressId, 301, mainWallettParams.password, function(uspeh1){
                                                                                     if(uspeh1){
                                                                                         res.status(200).json({ok: "access granted"});
                                                                                     }else{
                                                                                         res.status(500).json({error: "awward transaction failed!!"});
                                                                                     }
                                                                                     
                                                                                 })
                                                                                
                                                                            }
                                                                        });
                                                                    }else{
                                                                        res.status(500).json({error: "transaction failed"})
                                                                    }
                                                                })
                                                            }else{
                                                                res.status(403).json({error: "not enough coins to unlock this content"})
                                                            }
                                                        }else{
                                                            res.status(500).json({error: "can't get balance for this user!"});
                                                        }
                                                    })
                                                }
                                            });
                                            
                                        }
                                    })
                                    
                                }
                            })
                            
                            
                            
                            
                            
                        }else{
                            //poslji odgovor o napačnem geslu..
                            res.status(403).json({error: "incorrect password for your acc..Cant't buy content"})
                        }
                });
                
            }
        });
    }
}

module.exports.removeFile = function(req, res){
    let userID = req.body.userId;
    let contentID = req.body.contentId;
  
    if(!userID || !contentID){
        res.status(400).json({error: "parameters for content deletion not given"})
    }else{
            Content.findById(contentID).exec(function(error, content){
                if(error){
                    res.status(500).json(error);
                }else if(!content){
                    res.status(404).json({error: "can't find given content"});
                }else if(String(content.creator) !== String(userID)){
                    res.status(403).json({error: "you have to be to owner in you want to delete content"});
                }else{
                    Topic.findById(content.topicID).exec(function(err1, topic) {
                        if(err1){
                            res.status(500).json(err1);
                        }else{
                            //res.status(200).json(topic);
                            topic.contentTable.id(content._id).remove();
                            topic.save(function(err3){
                                if(err3){
                                    res.status(500).json(err3);
                                }else{
                                    fs.unlink("logic_api/contentFiles/"+userID+"/"+content.fileName, function(err) {
                                        if(err && err.code == 'ENOENT') {
                                            // file doens't exist
                                            console.info("File doesn't exist, won't remove it.");
                                            res.status(404).json({error: "file doesnt exist"});
                                        } else if (err) {
                                            // other errors, e.g. maybe we don't have enough permission
                                            console.error("Error occurred while trying to remove file");
                                            res.status(404).json(err);
                                        } else {
                                            console.info(`removed`);
                                            content.remove();
                                            res.status(200).json({ok: "file removed"});
                                        }
                                    });
                                }
                            });
                        }
                    })
                }
        });
    }

}

const makeTransaction = function(walletId, fromAddress, toAddress, amount, walletPassword, cb){
    let options, path;
    path = '/coinsAPI/operator/wallets/'+walletId+'/transactions';
    options = {
        url: apiParameters.server + path,
        method: 'POST',
        json: {
            fromAddress: fromAddress,
            toAddress: toAddress,
            amount: amount,
            changeAddress: fromAddress
        },
        headers: {
            'password': walletPassword
        }
    };
    request(options, callback);  
    function callback(error, response, body){
        if (!error && response.statusCode == 201) {
             cb(true); 
        }else{
            cb(false);
        }
    }
}

const alreadyAcces = function(accesArray, userID, cb){
    //console.log("USER: ",  userID);
    for(let i = 0; i < accesArray.length; i++){
        //console.log("KAJ: ",  accesArray[i].userID)
        if (String(accesArray[i].userID) === String(userID)){
            //console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            return cb(true);
        }
    }
    return cb(false);
}

const checkAllowedFileType = function(fileExtension){
    console.log(fileExtension)
    /*.docx, .xlsx, .pdf, .jpg, .png ,.txt, .pptx*/
    if(fileExtension === ".docx" || fileExtension === ".xlsx" || fileExtension === ".pdf" || fileExtension === ".jpg" || fileExtension === ".png" || fileExtension === ".txt" || fileExtension === ".pptx"){
        return true;
    }else{
        return false;
    }

}