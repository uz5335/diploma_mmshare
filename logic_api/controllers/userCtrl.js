const mongoose = require("mongoose");
const User = mongoose.model("User");
const Content = mongoose.model("Content");
const Topic = mongoose.model("Topic");
const makeDir = require("make-dir");
const request = require('request');
const emailValidator = require("email-validator");
var rmdir = require('rmdir');
const commonFunctions = require("../commonFunctions");

var bcrypt = require('bcrypt');

const apiParameters = commonFunctions.apiParameters;

let testPasswordHash = "";
module.exports.getUser = function(req, res){
    let userId = req.params.id;
    if(!userId){
        res.status(400).json({error: "missing userId"});
    }else{
        commonFunctions.searchUserById(userId, function(response, error){
            if(response && error){ //vse vredu
                res.status(200).json(response);
            }else if(response && !error){
                res.status(500).json(response);
            }else if(!response && !error){
                res.status(400).json({error: "can't find user"})
            }
        });
    }
}

module.exports.createUser = function(req, res){
    let username = req.body.username;
    let email = req.body.email;
    let password = req.body.password;
    

    
    if(!username || !email || !password){
        res.status(404).json({error: "missong arguments for creating new user"});
    }else if(!emailValidator.validate(email)){
        res.status(401).json({error: "incorrect email format"});
    }
    else if(password.length < 8 ){
        res.status(401).json({error: "password must be at least 8 characters long"});
    }else{
     User.findOne({username: req.body.username}).exec(function(error, _user){
         if(_user){
             res.status(400).json({error: "user with this username already exist"});
         }else if(error){
             res.status(500).json(error);
         }else{
             console.log('generating password');
              generatePassword(password, function(hash){
                 let options, path;
                path = '/coinsAPI/operator/wallets';
                options = {
                  url: apiParameters.server + path,
                  method: 'POST',
                  json: {
                      password: req.body.password
                  }
                };
                request(options, callback);  
               function callback(error, response, body){
                    if (!error && response.statusCode == 201) {
                        //res.status(200).json(body);
                        //console.log(body);
                        console.log("Pa to je nemogoče");
                        if(body.id && body.addresses[0]){
                            //console.log(body.id + "\n" + body.addresses[0]);
                            //res.status(200).json(body);
                            continueWithUser(req, res, body, hash);
                        }else{
                            res.status(500).json({error: "something went terrible wrong"});
                        }
                        
                    }else{
                        console.log("terrible wrong: ", error);
                        res.status(500).json(error)
                    }
                }
             })
         }
     });
    }
}

const continueWithUser = function(req, res, body, hash){
    let user = new User({username: req.body.username, email: req.body.email, password: hash , walletId: body.id, addressId: body.addresses[0],timeJoined: new Date()});
    user.save(function(error, responseUser){
        if(error){
            res.status(500).json(error);
        }else{
            makeDir('logic_api/contentFiles/' + responseUser._id).then(
                function(path){
                    console.log(path);
                    res.status(201).json({ok: "userCreated"});
                },
                function(error){
                    res.status(500).json({error: error});
                }
            );
        }
    });
}

module.exports.loginCheck = function(req, res){
    let userName = req.body.username;
    let password = req.body.password;
    //console.log("Password hash: ", testPasswordHash);

    if(!userName || !password){
        res.status(401).json({error: "username and password required"});
    }else{
        User.findOne({username: userName}).exec(function(error, user){
            if(!user){
                //pogledam se po emailu kako sem z loginom.
                User.findOne({email: userName}).exec(function(err, userMail){
                    if(!userMail){
                        res.status(404).json({error: "user doesn't exist"});
                    }
                    else if(err){
                        res.status(500).json(err);
                    }else{
                        passwordLoginCheck(req, res, userMail);
                    }
                });
                
            }else if(error){
                res.status(500).json(error);
            }else{
                //console.log(user.password + " and "+ password);
                passwordLoginCheck(req, res, user);
            }
        });
    }
}

const passwordLoginCheck = function(req, res, user){
    
    checkPassword(req.body.password, user.password, function(rs){
        if(rs){
             delete user.password;
              res.status(200).json(user);
        }else{
            res.status(401).json({error: "incorrect password"});
        }
        
    })
    
}

module.exports.changeDescription = function(req, res){
    let userId = req.params.id;
    let newDescription = req.body.description;
    
    if(!userId || !newDescription){
        res.status(400).json({error: "missing arguments"});
    }else{
        User.findById(userId).select("description").exec(function(error, user){
            if(!userId){
                res.status(404).json({error: "user can't be found"});
            }else if(error){
                res.status(500).json(error);
            }else{
                user.description = newDescription;
                user.save(function(err, userModified){
                    if(err){
                        res.status(404).json({error: "error on describtion update"})
                    }else{
                        res.status(200).json(userModified);
                    }
                })
            }
        });
    }
}

module.exports.changePassword = function(req, res){
    let userId = req.params.id;
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;
    //console.log(newPassword);
    if(!userId || !newPassword || !oldPassword){
        res.status(400).json({error: "missing arguments for password change"});
    }else if(newPassword.length < 8){
        res.status(400).json({error: "new password must be at least 8 characters long"});
    }
    else{
        User.findById(userId).exec(function(error, user){
            if(!user){
                res.status(404).json({error: "user not found"});
            }else if(error){
                res.status(500).json(error);
            }else{
                if(user.password !== oldPassword){
                    res.status(401).json({error: "wrong old password"})
                }else{
                    changeWalletPassword(user.walletId, oldPassword, newPassword, function(cbErr){
                        if(cbErr){
                            res.status(400).json({error: "password double check false"})
                        }else{
                            user.password = newPassword;
                            user.save(function(err, userModified){
                                if(err){
                                    res.status(400).json({error: "error on password change"});
                                }else{
                                    res.status(200).json({ok: "password change success"});
                                }
                            });
                        }
                    });
                    
                }
            }
        });
    }
}

const changeWalletPassword = function(walletId, oldPassword, newPassword, cb){
        let options, path;
        path = '/coinsAPI/operator/wallets/'+walletId+'/password';
        options = {
          url: apiParameters.server + path,
          method: 'POST',
          json: {
              oldPassword: oldPassword,
              newPassword: newPassword
          }
        };
        request(options, callback);  
       function callback(error, response, body){
            if (!error && response.statusCode == 200) {
               cb(false);
            }else{
                cb(true);
            }
        }
}

module.exports.getUserBalance = function(req, res){
    let userId = req.params.id;
    if(!userId){
        res.status(400).json({error: "no userId given"});
    }else{
        User.findById(userId).select("addressId").exec(function(error, user){
            if(!user){
                res.status(404).json({error: "user with given id not found"});
            }else if(error){
                res.status(500).json(error);   
            }else{
                commonFunctions.getBalanceFunction(user.addressId, function(body){
                    //let neki = body.balance;
                    //console.log(neki);
                    if(body){
                        res.status(200).json(body);
                    }else{
                        res.status(404).json({error: "not found address for given user"});
                    }
                })
            }
        });
    }
}

module.exports.deleteUser = function(req, res){
    let userId = req.body.userId;
    if(!userId){
        res.status(400).json({error: "missgin reqeust parameter userId"});
    }else{
        User.findByIdAndRemove(userId).exec(function(error, user){
            if(error){
                res.status(500).json({error: "something went horrible wrong when searhing for user to delete"});
            }else if(!user){
                res.status(404).json({error: "can't find given user"});
            }else{
                Content.remove({creator: userId}, function(err){
                    if(err){
                        res.status(400).json({error: "error when removing user content"});
                    }else{
                        //res.status(200).json({ok: "ok"});
                        Topic.find().exec(function(err3, topics){
                            if(err3){
                                res.status(500).json(err3);
                            }else{
                                for(let i = 0; i < topics.length; i++){
                                    
                                    for(let j = 0; j < topics[i].contentTable.length; j++){
                                        if(String(topics[i].contentTable[j].userId) === String(userId)){
                                            topics[i].contentTable.splice(j, 1); //odstrani tega...
                                        }
                                    }
                                    
                                    topics[i].save(function(err5, ok){
                                        if(err5){
                                            return res.status(500).json(err5);
                                        }
                                    });
                                }
                                //zdaj pa se fizicno datoteke odstranit oz celo mapo..
                                rmdir('logic_api/contentFiles/'+userId, function(err, dirs, files){
                                    if(err){
                                        res.status(500).json(err);
                                    }else{
                                        res.status(200).json({ok: "user deleted"});
                                    }
                                })
                            }
                        });
                    }
                });
            }
        });
    }
}


const someOtherPlaintextPassword = 'tets';



let generatePassword = function (plainPassowrd, cb){
    
    bcrypt.hash(plainPassowrd, 10, function(err, hash) {
        if(err){
            cb(null);
        }else{
            cb(hash);
        }
        
    });
    
}

let checkPassword = function(plainPassowrd, passwordHash, cb){
    bcrypt.compare(plainPassowrd, passwordHash, function(err, res) {
    // res == true
        cb(res);
});
}