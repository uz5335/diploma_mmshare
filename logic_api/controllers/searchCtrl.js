const mongoose = require("mongoose");
const User = mongoose.model("User");
const Topic = mongoose.model("Topic");
const Content = mongoose.model("Content");

module.exports.searchUserContentByUsername = function(req, res){
    let userName = req.params.userName;
    if(!userName){
        res.status(400).json({error: "username not given"});
    }
    User.findOne({username: userName}).exec(function(error, user){
        if(!user){
            res.status(404).json({error: "user not found"});
        }else if(error){
            res.status(500).json(error);
        }else{
            getUserContent(user._id, function(err3, result){
                if(err3){
                    res.status(500).json(err3);
                }else{
                    res.status(200).json(result);
                }
            });
        }
    });
}

module.exports.searchUserById = function(req, res){
    let id = req.params.id;
    if(!id){
        res.status(400).json({error: "no search params given"});
    }else{
        User.findById(id).exec(function(error, user){
            if(!user){
                res.status(404).json({error: "can't find user with given id!"});
            }else if(error){
                res.status(500).json(error);
            }else{
                getUserContent(user._id, function(err3, result){
                    if(err3){
                        res.status(500).json(err3);
                    }else{
                        res.status(200).json(result);
                    }
                });
            }
        });
    }
}

module.exports.searchContentInTopic = function(req, res){
    let topicName = req.params.topicName;
    if(!topicName){
        res.status(400).json({error: "parameter for request not given"});
    }else{
        Topic.findOne({name: topicName}).exec(function(error, topic){
            if(!topic){
                res.status(404).json({error: "topic with given name doesn't exit"});
            }else if(error){
                res.status(500).json(error);
            }else{
                Content.find({topicID: topic._id}).populate('creator', 'username').populate('topicID', 'name').exec(function(err, content){
                    if(err){
                        res.status(500).json(err);
                    }else{
                        res.status(200).json(content);
                    }
                });
            }
        })
    }
}

module.exports.getAllTopics = function(req, res){
    Topic.find().select("name").exec(function(error, topic){
        if(error){
            res.status(404).json({error: "something wnet wrong with all topic search"});
        }else{
            res.status(200).json(topic);
        }
    });
}


module.exports.getAllAccessContent = function(req, res){
    let userId = req.params.userId;
    if(!userId){
        res.status(404).json({error: "no user id given"});
    }
    Content.find({"access.userID": {$in: [userId]}}).populate('creator', 'username').populate('topicID', 'name').exec(function(err, content){
        if(err){
            res.status(500).json(err);
        }else{
            console.log(content.length);
            res.status(200).json(content);
        }
    });
}

const getUserContent = function(userId, cb){
    Content.find({creator: userId}).populate('creator', 'username').populate('topicID', 'name').exec(function(err, content){
        if(err){
            cb(err, null);
        }else{
            cb(null, content);
        }
    });
}


module.exports.getUserDataName = function(req, res){
        let userName = req.params.userName;
    if(!userName){
        res.status(400).json({error: "username not given"});
    }
    User.findOne({username: userName}).exec(function(error, user){
        if(!user){
            res.status(404).json({error: "user not found"});
        }else if(error){
            res.status(500).json(error);
        }else{
            res.status(200).json((user));
        }
    });
}


module.exports.getContentById = function(req, res){
    let contentId = req.params.id;
    if(!contentId){
        res.status(400).json({error: "no contetndID given"});
    }else{
        Content.findById(contentId).populate('creator', 'username').populate('topicID', 'name').exec(function(error, content){
            if(error){
                res.status(500).json(error);
            }else if(!content){
                res.status(400).json("no content found");
            }else{
                res.status(200).json(content);
            }
        });
    }
    
}