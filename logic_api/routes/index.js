
var express = require('express');
var router = express.Router();

var userController = require("../controllers/userCtrl");
var contentControll = require("../controllers/contentTopicCtrl");
var searchControll = require("../controllers/searchCtrl");

//user controll
router.get('/getUser/:id', userController.getUser);
router.get('/getUserBalance/:id', userController.getUserBalance);
router.post('/createUser', userController.createUser);
router.post('/login', userController.loginCheck);
router.put('/changeDescription/:id', userController.changeDescription);
router.put('/changePassword/:id', userController.changePassword);
router.delete('/deleteUser', userController.deleteUser);

//contentControll
router.post('/fileUpload', contentControll.fileUpload);
router.get('/fileDownload/:id/:fileId', contentControll.fileDownload);
router.post('/buyContent', contentControll.buyContent);
router.delete('/deleteContent', contentControll.removeFile);

//topic
router.post('/createTopic', contentControll.createTopic);

//search
router.get('/searchByUserName/:userName', searchControll.searchUserContentByUsername);
router.get('/userDataByUserName/:userName', searchControll.getUserDataName);
router.get('/searchByUserId/:id', searchControll.searchUserById);
router.get('/searchContentInTopic/:topicName', searchControll.searchContentInTopic);
router.get('/getAllTopics', searchControll.getAllTopics);
router.get('/getAllAccessTopic/:userId', searchControll.getAllAccessContent);
router.get('/findContentById/:id', searchControll.getContentById);

module.exports = router;