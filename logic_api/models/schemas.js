//{}
const mongoose = require("mongoose"),
    Schema = mongoose.Schema;

mongoose.Promise = global.Promise;

const userSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    password:{
        type: String,
        minLength: 8,
        maxLength: 100,
        required: true
    }, 
    walletId: {
        type: String,/* potencialno vec walletov, da imaš sam realno zelo tezko, moznost kasneje za upgrade kasneje, če si zelijo userji narediti vec naslovov za transakcije med njimi,
                        sam to se lahko zelo zakomplicira...tako da malo vrjetno..
                        index == 0 --> main Adress */
        required: true
    },
    addressId: {
        type: String,
        required: true
    },
    description: {
        type: String,
        minLength: 1,
        maxLength: 250,
        "default": "NaN",
    },
    timeJoined: {
        type: Date,
        required: true,
        "default": Date.now
        
    }
},
    {
        versionKey: false
    });

const accessSchema = new Schema(
    {
        //id od userja tisti ki ima dostop  do tega.
        userID: {
            type: Schema.ObjectId,
            required: true,
            ref: "User"
        },
        timeAssigned:{
            type: Date,
            required: true,
            "default": Date.now
        }
    },
    {
    versionKey: false
});

//LOCATION;
const contentSchema = new Schema(
    {
        //da vemo od koga je vsebina.
                /*
        ./files/:laction/fileName.pdf --> full FileName tudi s končno pripono glede kolicine fila.
        */
        creator: {
          type: Schema.ObjectId,
          required: true,
          ref: "User"
        },
        topicID: {
            type:Schema.ObjectId,
            required: true,
            ref: "Topic"
        },
        fileName: {
            type: String,
            required: true
        },
        price: {
            // ta del se bo zracunal sproti, preden se shrani v bazo se cena izracuna glede na velikost datoteke.
            type: Number,
            required: true
        },
        mimetype: {
            type: String,
            required: true
        },
        access: {
            type: [accessSchema]  //TODO naredi access schema.
        },
        timeCreated: {
            type: Date,
            required: true,
            "default": Date.now
        }
    },
    {
    versionKey: false
});


const topicsSchema = new Schema(
    {
        //dobimo refernco kater vse kontent spade pod doloceno temo.
        name: {
            type: String,
            required: true,
        },
        contentTable: [{
            _id:{
                type: Schema.ObjectId,
                required: true,
                ref: "Content"
            },
            userId: {
                type: Schema.ObjectId,
                required: true,
                ref: "User"
            },
            timeAdded: {
                type: Date,
                required: true,
                "default": Date.now
            }
        }],
        timeCreated: {
            type: Date,
            required: true,
            "default": Date.now
        }
    },
    {
    versionKey: false
});

mongoose.model("User", userSchema);
mongoose.model("Content", contentSchema);
mongoose.model("Topic", topicsSchema);

console.log('schemas created');
