//HINT ZA TRANSAKCIJO POTREBUJEM EN KOVANEC VISKA

const R = require('ramda');
const path = require('path');
const Block = require('../lib/blockchain/block');
const Transaction = require('../lib/blockchain/transaction');
const TransactionAssertionError = require('../lib/blockchain/transactionAssertionError');
const BlockAssertionError = require('../lib/blockchain/blockAssertionError');

const ArgumentError = require('../lib/util/argumentError');
const CryptoUtil = require('../lib/util/cryptoUtil');
const timeago = require('timeago.js');

const Blockchain = require('../lib/blockchain');
const Operator = require('../lib/operator');
const Miner = require('../lib/miner');
const Node = require('../lib/node');

var os = require("os");
var host = os.hostname();

const port = process.env.PORT || '3000';
const peers = [];
const logLevel = 6 
const name =  '1';

require('../lib/util/consoleWrapper.js')(name, logLevel);

console.info(`Starting node ${name}`);

let blockchain = new Blockchain(name);
let operator = new Operator(name, blockchain);
let miner = new Miner(blockchain, logLevel);
let node = new Node(host, port, peers, blockchain);

let mainWalletId = "b5ebd215a8beb5eafe6e886ba5407ff2411f57ddeb80c91e6e32d463fd42889a";
let mainAddressHash = "219a3240c3ca75363f2417080d17c1e5c015436ef05eca82fdd6fb410ad32965";


const projectWallet = (wallet) => {
    return {
        id: wallet.id,
        addresses: R.map((keyPair) => {
            return keyPair.publicKey;
        }, wallet.keyPairs)
    };
};

//////////////////////////////////////////////////////////////////////
module.exports.getBlockChain = function(req, res) {
     res.status(200).send(blockchain.getAllBlocks());
};
module.exports.getLastBlock = function(req, res){
    let lastBlock = blockchain.getLastBlock();
    if (lastBlock == null) res.status(404).json({error: "last block not found"});
    res.status(200).send(lastBlock);
}
module.exports.blockWithHash = function(req, res){
    let blockFound = blockchain.getBlockByHash(req.params.hash);
    if (blockFound == null)  res.status(404).json({error: `Block not found with hash: '${req.params.hash}'`});
    res.status(200).send(blockFound);
}
module.exports.blockWithIndex = function(req, res){
    let blockFound = blockchain.getBlockByIndex(parseInt(req.params.index));
    if (blockFound == null) res.status(404).json({error: `Block not found with index: '${req.params.index}'`});
    res.status(200).send(blockFound);
}
module.exports.unspentTransactions = function(req, res){
    res.status(200).send(blockchain.getUnspentTransactionsForAddress(req.query.address));
}

///////////////////////////////FUNKCIONALNI DEL CELOTNEGA SISTEMA
module.exports.getWallets = function(req, res){
    let wallets = operator.getWallets();
    let projectedWallets = R.map(projectWallet, wallets);
    res.status(200).send(projectedWallets);
}


module.exports.createWallet = function(req, res){
    let password = req.body.password;
    console.log('coins API: ', password);
    if(!password) res.status(404).json({error: "missing arguments to create new wallet"});
    if (password.length < 8 ) res.status(400).json({error: "password must be at least 8 char long"});
    let newWallet = operator.createWalletFromPassword(password);
    let newAddress=operator.generateAddressForWallet(newWallet.id);
    let projectedWallet = projectWallet(newWallet);
    //console.log(projectedWallet);
    //0-ta transakcija
     //res.status(201).send(projectedWallet);
    try{
        //console.log(newAddress);
        let newTransaction = operator.createTransaction(mainWalletId,
                        mainAddressHash, newAddress, 5001, mainAddressHash); //torej ko uporabnik ustvari racun dobi 2001 kovancev za porabit, s tem lahko nekaj vsebin na zacetku prenese...
        newTransaction.check();
        let transactionCreated = blockchain.addTransaction(Transaction.fromJson(newTransaction));
        res.status(201).send(projectedWallet);
    }catch(ex){
        res.status(400).json({error:  "error"});
    }

}

module.exports.createTransaction = function(req, res){
    let walletId = req.params.walletId;
    let password = req.headers.password;

    if (!password) {
        res.status(401).json({error: `Wallet\'s password is missing`});
    }
    else{
        let passwordHash = CryptoUtil.hash(password);
        if (!operator.checkWalletPassword(walletId, passwordHash)){
          res.status(403).json({error: `Invalid password for wallet '${walletId}'`});  
        }else{
            try {
                 //torej ce gre transakcija v drugo smer da uporabnik nalaga gor stvari
                if(walletId !== mainWalletId){
                     let balance = operator.getBalanceForAddress(req.body.fromAddress);
                     if(balance <= req.body.amount){
                         res.status(400).json({error: "not enough coins in your wallet"});
                     }else{
                          //uporabnik nalaga gor//kupuje in ni potrebno gledat drugih stvari..
                         let newTransaction = operator.createTransaction(walletId, req.body.fromAddress, req.body.toAddress, req.body.amount, req.body['changeAddress'] || req.body.fromAddress);
                        newTransaction.check();
                        let transactionCreated = blockchain.addTransaction(Transaction.fromJson(newTransaction));
                        console.log("nalam/kupujem");
                        res.status(201).send(transactionCreated);
                     }
                   
                }else{
                    //preveri se kako je z mainAddress in kolicino denarja na naslovu.
                     let balance = operator.getBalanceForAddress(mainAddressHash); //poglej razmerje znotraj glavne denarnice...
                     if(balance <= req.body.amount){ //ce je manjsi naredi transakcijo.
                         getNewCoins(function(block){
                             if(block){
                                let newTransaction = operator.createTransaction(walletId, req.body.fromAddress, req.body.toAddress, req.body.amount, req.body['changeAddress'] || req.body.fromAddress);
                                newTransaction.check();
                                let transactionCreated = blockchain.addTransaction(Transaction.fromJson(newTransaction));
                                res.status(201).send(transactionCreated);
                             }else{
                                 //do tega dela naceloma nikoli ne smes pridt..
                                 res.status(500).json({error:  "something went horrible wrong"});
                             }
                         });
                     }else{
                        let newTransaction = operator.createTransaction(walletId, req.body.fromAddress, req.body.toAddress, req.body.amount, req.body['changeAddress'] || req.body.fromAddress);
                        newTransaction.check();
                        let transactionCreated = blockchain.addTransaction(Transaction.fromJson(newTransaction));
                        res.status(201).send(transactionCreated);
                     }    
                }
            } catch (ex) {
                if (ex instanceof ArgumentError || ex instanceof TransactionAssertionError) res.status(400).json({error:  ex.message + " " +  walletId + " " + ex});
                else throw ex;
            }
        } 
    }
}
module.exports.getBalance = function(req, res){
    let addressId = req.params.addressId;

    try {
        let balance = operator.getBalanceForAddress(addressId);
        //console.log(balance);
        res.status(200).json({ balance: balance });
    } catch (ex) {
        if (ex instanceof ArgumentError) res.status(400).json({error:  ex.message + " " +  addressId + " " + ex});
        else throw ex;
    }
}

module.exports.changePassword = function(req, res){
    let wallet = operator.getWalletById(req.params.walletId);
    let oldPassword = req.body.oldPassword;
    let newPassword = req.body.newPassword;
    
    if(!oldPassword || !newPassword){
      res.status(404).json({error: "missing requestParams"});  
    } else{
        if (wallet == null){
          res.status(404).json({error: `Wallet not found with id '${req.params.walletId}'`});  
        }else{
            let passwordHash = CryptoUtil.hash(oldPassword);
            if (!operator.checkWalletPassword(wallet.id, passwordHash)) {
                res.status(403).json({error: `Invalid password for wallet '${wallet.id}'`});   
            }else{
                if(newPassword.length < 8) {
                    res.status(400).json({error: "password must be at least 8 char long"});   
                }else{
                    wallet.passwordHash = CryptoUtil.hash(newPassword);
                    res.status(200).json({ok: "new Password set!"});
                }
            }
            
        } 
        
    }

}

let getNewCoins = function(callback){
        miner.mine(mainAddressHash, mainAddressHash)
        .then((newBlock) => {
            newBlock = Block.fromJson(newBlock);
            blockchain.addBlock(newBlock);
            callback(newBlock);
        })
        .catch((ex) => {
           callback(null);
        });
}

///////////////////////////////////////////123456///////////////////KONEC FUNKCIONALNEGA DELA
///LEGACY

/*module.exports.createAddressForWallet = function(req, res){
   let walletId = req.params.walletId;
    let password = req.headers.password;

    if (password == null) res.status(401).json({error: `Wallet\'s password is missing`});
    let passwordHash = CryptoUtil.hash(password);

    try {
        if (!operator.checkWalletPassword(walletId, passwordHash)) res.status(403).json({error: `Invalid password for wallet '${walletId}'`});

        let newAddress = operator.generateAddressForWallet(walletId);
        res.status(201).send({ address: newAddress });
    } catch (ex) {
        if (ex instanceof ArgumentError) res.status(400).json({error:  ex.message + " " +  walletId + " " + ex});
        else throw ex;
    }
}*/

/*module.exports.getWalletById = function(req, res){
     let walletFound = operator.getWalletById(req.params.walletId);
    if (walletFound == null) res.status(404).json({error: `Wallet not found with id '${req.params.walletId}'`});
    let projectedWallet = projectWallet(walletFound);
    res.status(200).send(projectedWallet);
}*/

module.exports.miner = function(req, res, next){
        miner.mine(mainAddressHash, mainAddressHash)
        .then((newBlock) => {
            newBlock = Block.fromJson(newBlock);
            blockchain.addBlock(newBlock);
            res.status(201).send(newBlock);
        })
        .catch((ex) => {
           res.status(409).json({error:  "Mining error: \n"+ ex});
        });
}