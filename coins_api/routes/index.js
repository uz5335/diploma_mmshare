/*
main pass: VSCcn2wM
*/

var express = require('express');
var router = express.Router();
//console.log(port);

var mainCtrl = require('../controllers/mainCtrl');

//BOLJ UTILITI, KJER SE LAHKO STVARI PREVERJA...
//dobim celotno verigo blokov
/*router.get('/blockchain/blocks', mainCtrl.getBlockChain);
//dobim zadnji block
router.get('/blockchain/blocks/latest', mainCtrl.getLastBlock);
//lahko dobim cel block by hash
router.get('/blockchain/blocks/:hash([a-zA-Z0-9]{64})',mainCtrl.blockWithHash); 
//dobim cel block po indexu
router.get('/blockchain/blocks/:index', mainCtrl.blockWithIndex);
//preveri unspent transactions
router.get('/blockchain/transactions/unspent', mainCtrl.unspentTransactions);*/

//////////////////////---------------------------------------------------------------------
/*
OPERATIVNI DEL
*/

router.get('/operator/wallets', mainCtrl.getWallets);  //GET ALL WALLETS

router.post('/operator/wallets', mainCtrl.createWallet);   //CREATE NEW WALLET, REQURES PASSWORD

//router.get('/operator/wallets/:walletId', mainCtrl.getWalletById); //GET WALLET BY ID

router.post('/operator/wallets/:walletId/transactions', mainCtrl.createTransaction); //CREATE NEW TRANSACTION

//router.post('/operator/wallets/:walletId/addresses', mainCtrl.createAddressForWallet); // CREATE NEW ADDRESS FOR A WALLET --- 
                                                                          
router.get('/operator/:addressId/balance', mainCtrl.getBalance); //GET BALANCE FOR A WALLET

//router.post('/miner/mine',  mainCtrl.miner);

router.post('/operator/wallets/:walletId/password', mainCtrl.changePassword);

module.exports = router;