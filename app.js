var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
var helmet = require('helmet');

require('./logic_api/models/db');

 var indexApiCoins = require('./coins_api/routes/index');
 var logicApi = require('./logic_api/routes/index');

 var adminAPI = require("./admin_api");
 var appServer = require('./app_server/routes/index.js');
 
var app = express();

app.use(helmet());
app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'DENY');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});

// view engine setup
app.set('views', path.join(__dirname, 'app_server', 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(fileUpload());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, 'CLIENT_REACT')));

app.use('/coinsAPI', indexApiCoins);
app.use('/logicAPI', logicApi);
app.use("/adminAPI", adminAPI);

app.use("/admin", appServer);

app.use(function (req, res) {
    res.sendFile(path.join(__dirname, 'CLIENT_REACT', 'index.html')); //poslji datoteko index.html.
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
    

module.exports = app;
